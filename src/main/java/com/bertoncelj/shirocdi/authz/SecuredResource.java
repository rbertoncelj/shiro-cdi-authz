package com.bertoncelj.shirocdi.authz;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.*;

/**
 * @author Rok Bertoncelj
 */
@Inherited
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface SecuredResource {
    /**
     * Resource name that will form a permission string together with action name.
     *
     * @return
     */
    @Nonbinding
    String value() default "";

    /**
     * Should methods without @{@link SecuredAction} be secured or not
     *
     * @return
     */
    @Nonbinding
    boolean securedByDefault() default true;
}
