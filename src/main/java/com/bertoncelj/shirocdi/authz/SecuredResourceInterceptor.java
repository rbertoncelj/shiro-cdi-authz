package com.bertoncelj.shirocdi.authz;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * @author Rok Bertoncelj
 */
@Slf4j
@NoArgsConstructor
@Dependent
@SecuredResource
@Interceptor
@Priority(value = Interceptor.Priority.APPLICATION)
public class SecuredResourceInterceptor {
    private PermissionExtractor permissionExtractor;
    private Instance<Subject> subjectInstance;

    @Inject
    public SecuredResourceInterceptor(PermissionExtractor permissionExtractor, @ShiroCdi Instance<Subject> subjectInstance) {
        this.permissionExtractor = permissionExtractor;
        this.subjectInstance = subjectInstance;
    }

    @AroundInvoke
    public Object authorize(InvocationContext invocationContext) throws Exception {
        // get subject
        Subject subject = subjectInstance.get();
        if (subject == null) {
            throw new AuthorizationException("No subject is present!");
        }
        if (!subject.isAuthenticated()) {
            throw new AuthorizationException("Subject is not authenticated!");
        }

        // extract and check permission
        String permission = permissionExtractor.extract(invocationContext);
        if (permission != null) {
            subject.checkPermission(permission);
        }

        // proceed if allowed
        return invocationContext.proceed();
    }
}
