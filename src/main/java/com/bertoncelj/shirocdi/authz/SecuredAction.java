package com.bertoncelj.shirocdi.authz;

import java.lang.annotation.*;

/**
 * @author Rok Bertoncelj
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface SecuredAction {
    /**
     * Action name that will form a permission string together with resource name.
     *
     * @return
     */
    String value() default "";

    /**
     * If <code>true</code>, the name from <code>@SecureResource</code> will not be used as a prefix for final
     * permission string. Instead, the value of this annotation alone will directly be used as a permission string.
     *
     * @return
     */
    boolean absolute() default false;
}
