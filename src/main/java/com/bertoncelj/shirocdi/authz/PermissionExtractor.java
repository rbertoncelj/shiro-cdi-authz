package com.bertoncelj.shirocdi.authz;

import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;

/**
 * @author Rok Bertoncelj
 */
@Slf4j
public class PermissionExtractor {

    public String extract(InvocationContext invocationContext) {
        Method method = invocationContext.getMethod();
        Class<?> clazz = method.getDeclaringClass();

        return extract(clazz, method);
    }

    protected String extract(Class<?> clazz, Method method) {
        // get action name from annotation or generate one if necessary
        SecuredAction securedAction = method.getAnnotation(SecuredAction.class);
        String actionName = securedAction != null ? securedAction.value() : null;
        if (Strings.isNullOrEmpty(actionName)) {
            actionName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, method.getName());
            log.debug("Generated action name '{}' for method {}", actionName, method);
        }

        // if absolute, we use only the action name, otherwise we combine with resource name
        if (securedAction != null && securedAction.absolute()) {
            return actionName;
        }

        // if no @SecuredAction on method and secureByDefault==false, no authorization is required
        SecuredResource securedResource = clazz.getAnnotation(SecuredResource.class);
        if (!securedResource.securedByDefault() && securedAction == null) {
            return null;
        }

        // get resource name from annotation or generate one if necessary
        String resourceName = securedResource.value();
        if (Strings.isNullOrEmpty(resourceName)) {
            resourceName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, clazz.getSimpleName());
            log.debug("Generated resource name '{}' for class {}", resourceName, clazz);
        }

        return resourceName + ":" + actionName;
    }
}
