package com.bertoncelj.shirocdi.authz;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.enterprise.inject.Produces;

import static org.testng.Assert.fail;

/**
 * @author Rok Bertoncelj
 */
public class SecuredInterceptorIT {
    private Weld weld;
    private WeldContainer container;
    private TestBean testBean;


    @AfterMethod
    public void tearDown() throws Exception {
        SecurityUtils.getSubject().logout();
    }

    @BeforeClass
    public void setUpClass() throws Exception {
        // Shiro
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro-test.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        // Weld
        weld = new Weld().packages(SecuredResourceInterceptor.class);
        container = weld.initialize();

        // Get instance of TestBean via CDI
        testBean = container.select(TestBean.class).get();
    }

    @AfterClass
    public void tearDownClass() throws Exception {
        container.shutdown();
    }


    @Test
    public void testAdmin() throws Exception {
        SecurityUtils.getSubject().login(new UsernamePasswordToken("root", "rootPwd"));
        testBean.hello();
        testBean.bye();
    }

    @Test
    public void testGuest() throws Exception {
        SecurityUtils.getSubject().login(new UsernamePasswordToken("anonymous", "anonymousPwd"));
        testBean.hello();
        try {
            testBean.bye();
            fail();
        } catch (AuthorizationException e) {
            // expected
        }
    }

    @Test(expectedExceptions = AuthorizationException.class)
    public void testUnauthenticated() throws Exception {
        testBean.hello();
    }


    public static class SubjectProvider {
        @Produces
        @ShiroCdi
        public Subject getSubject() {
            return SecurityUtils.getSubject();
        }
    }

    @SecuredResource
    public static class TestBean {
        public String hello() {
            return "Hello!";
        }

        public String bye() {
            return "Bye!";
        }
    }
}
