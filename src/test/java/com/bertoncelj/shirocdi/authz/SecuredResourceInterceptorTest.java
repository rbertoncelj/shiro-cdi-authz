package com.bertoncelj.shirocdi.authz;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.enterprise.inject.Instance;
import javax.interceptor.InvocationContext;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.fail;

/**
 * @author Rok Bertoncelj
 */
public class SecuredResourceInterceptorTest {
    @Mock
    private PermissionExtractor permissionExtractor;
    @Mock
    private Instance<Subject> subjectInstance;
    @Mock
    private Subject subject;
    @Mock
    private InvocationContext invocationContext;
    @InjectMocks
    private SecuredResourceInterceptor securedResourceInterceptor;


    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(subjectInstance.get()).thenReturn(subject);
        when(permissionExtractor.extract(any())).thenReturn("foo:bar");
        when(subject.isAuthenticated()).thenReturn(true);
    }


    /**
     * Tests that invocation proceeds when permission check succeeds.
     */
    @Test
    public void testPermissionCheckSuccess() throws Exception {
        securedResourceInterceptor.authorize(invocationContext);

        verify(permissionExtractor).extract(invocationContext);
        verify(subject).checkPermission("foo:bar");
        verify(invocationContext, times(1)).proceed();
    }

    /**
     * Tests that invocation does not proceed when permission check fails.
     */
    @Test
    public void testPermissionCheckFailure() throws Exception {
        doThrow(AuthorizationException.class).when(subject).checkPermission("foo:bar");

        try {
            securedResourceInterceptor.authorize(invocationContext);
            fail();
        } catch (AuthorizationException e) {
            // expected
        }

        verify(permissionExtractor).extract(invocationContext);
        verify(subject).checkPermission("foo:bar");
        verify(invocationContext, never()).proceed();
    }

    /**
     * Tests that authorization fails instantly if there is no current subject present.
     */
    @Test(expectedExceptions = AuthorizationException.class, expectedExceptionsMessageRegExp = ".*present.*")
    public void testFailOnMissingSubject() throws Exception {
        when(subjectInstance.get()).thenReturn(null);
        securedResourceInterceptor = new SecuredResourceInterceptor(null, subjectInstance);

        securedResourceInterceptor.authorize(null);
    }

    /**
     * Tests that authorization fails if instantly if subject is not authenticated.
     */
    @Test(expectedExceptions = AuthorizationException.class, expectedExceptionsMessageRegExp = ".*authenticated.*")
    public void testFailOnUnauthenticatedSubject() throws Exception {
        when(subject.isAuthenticated()).thenReturn(false);
        securedResourceInterceptor.authorize(null);
    }

    /**
     * Tests that invocation is allowed if no permission is required.
     */
    @Test
    public void testAllowOnNullPermission() throws Exception {
        when(permissionExtractor.extract(any())).thenReturn(null);
        securedResourceInterceptor.authorize(invocationContext);
    }
}