package com.bertoncelj.shirocdi.authz;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.interceptor.InvocationContext;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * @author Rok Bertoncelj
 */
public class PermissionExtractorTest {
    @Spy
    private PermissionExtractor permissionExtractor;
    @Mock
    private InvocationContext invocationContext;


    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testExtractInvocationContext() throws Exception {
        when(invocationContext.getMethod()).thenReturn(FooBarOne.class.getDeclaredMethod("doSomethingFoo"));
        when(invocationContext.getTarget()).thenReturn(new FooBarOne());
        when(permissionExtractor.extract(invocationContext.getTarget().getClass(), invocationContext.getMethod()))
                .thenReturn("mock:mock");

        assertEquals(permissionExtractor.extract(invocationContext), "mock:mock");
        verify(permissionExtractor, times(1)).extract(invocationContext.getTarget().getClass(), invocationContext.getMethod());
    }

    @Test
    public void testExtract() throws Exception {
        String permission = permissionExtractor.extract(FooBarOne.class, FooBarOne.class.getDeclaredMethod("doSomethingFoo"));
        assertEquals(permission, "foo-resource:foo-method");
    }

    @Test
    public void testExtractAbsolute() throws Exception {
        String permission = permissionExtractor.extract(FooBarOne.class, FooBarOne.class.getDeclaredMethod("doSomethingBar"));
        assertEquals(permission, "how:now:brown:cow");
    }

    @Test
    public void testExtractAbsoluteGenerated() throws Exception {
        String permission = permissionExtractor.extract(FooBarOne.class, FooBarOne.class.getDeclaredMethod("doSomethingFooBar"));
        assertEquals(permission, "do_something_foo_bar");
    }

    @Test
    public void testExtractGenerated() throws Exception {
        String permission = permissionExtractor.extract(FooBarTwo.class, FooBarTwo.class.getDeclaredMethod("doSomethingBar"));
        assertEquals(permission, "foo_bar_two:do_something_bar");
    }

    @Test
    public void testExtractExtended() throws Exception {
        String permission = permissionExtractor.extract(FooBarExtended.class, FooBarExtended.class.getMethod("doSomethingFoo"));
        assertEquals(permission, "foo-resource:foo-method");
    }

    @Test
    public void testNotSecuredByDefault() throws Exception {
        String permission = permissionExtractor.extract(FooBarOne.class, FooBarOne.class.getMethod("doSomethingUnsecured"));
        assertNull(permission);
    }

    @SecuredResource(value = "foo-resource", securedByDefault = false)
    class FooBarOne {
        @SecuredAction("foo-method")
        public void doSomethingFoo() {
        }

        @SecuredAction(value = "how:now:brown:cow", absolute = true)
        public void doSomethingBar() {
        }

        @SecuredAction(absolute = true)
        public void doSomethingFooBar() {
        }

        public void doSomethingUnsecured() {
        }
    }

    @SecuredResource
    class FooBarTwo {
        public void doSomethingBar() {
        }
    }

    class FooBarExtended extends FooBarOne {
    }

}